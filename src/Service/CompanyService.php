<?php


namespace App\Service;


use App\Entity\Company;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;

class CompanyService
{
    /**
     * @var ParameterBagInterface
     */
    private $params;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var LoadFileService
     */
    private $loadFileService;

    public function __construct(EntityManagerInterface $entityManager,
                                ParameterBagInterface $params,
                                LoadFileService $loadFileService)
    {
        $this->params = $params;
        $this->entityManager = $entityManager;
        $this->loadFileService = $loadFileService;
    }

    public function editCompany(FormInterface $form, Company $company)
    {
        $logo = $form['logo']->getData();
        $path = $this->params->get('company_logo_directory');
        if ($logo) {
            $newFilename = $this->loadFileService->loadFile($logo, $path);

            $company->setLogo($newFilename);
        }

        $this->entityManager->persist($company);
        $this->entityManager->flush();
    }

}