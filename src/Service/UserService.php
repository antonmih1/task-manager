<?php


namespace App\Service;


use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

class UserService
{
    /**
     * @var ParameterBagInterface
     */
    private $params;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var Security
     */
    private $security;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var Mailer
     */
    private $mailer;
    /**
     * @var CodeGenerator
     */
    private $codeGenerator;

    public function __construct(EntityManagerInterface $entityManager,
                                ParameterBagInterface $params,
                                Security $security,
                                UserPasswordEncoderInterface $passwordEncoder,
                                Mailer $mailer,
                                CodeGenerator $codeGenerator)
    {
        $this->params = $params;
        $this->entityManager = $entityManager;
        $this->security = $security;
        $this->passwordEncoder = $passwordEncoder;
        $this->mailer = $mailer;
        $this->codeGenerator = $codeGenerator;
    }

    public function createUser(FormInterface $form,
                               User $user)
    {
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                $form->get('firstName')->getData()
            )
        );

        $user->setConfirmationCode($this->codeGenerator->getConfirmationCode());
        $user->setCompany($this->security->getUser()->getCompany());

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->mailer->sendConfirmationMessage($user);
    }
}