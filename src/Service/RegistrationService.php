<?php


namespace App\Service;


use App\Entity\User;
use App\Security\UserAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class RegistrationService
{
    /**
     * @var ParameterBagInterface
     */
    private $params;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var GuardAuthenticatorHandler
     */
    private $guardHandler;
    /**
     * @var UserAuthenticator
     */
    private $authenticator;
    /**
     * @var Mailer
     */
    private $mailer;
    /**
     * @var CodeGenerator
     */
    private $codeGenerator;
    /**
     * @var CompanyService
     */
    private $companyService;

    public function __construct(EntityManagerInterface $entityManager,
                                ParameterBagInterface $params,
                                UserPasswordEncoderInterface $passwordEncoder,
                                GuardAuthenticatorHandler $guardHandler,
                                UserAuthenticator $authenticator,
                                Mailer $mailer,
                                CodeGenerator $codeGenerator,
                                CompanyService $companyService)
    {
        $this->params = $params;
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->guardHandler = $guardHandler;
        $this->authenticator = $authenticator;
        $this->mailer = $mailer;
        $this->codeGenerator = $codeGenerator;
        $this->companyService = $companyService;
    }

    public function register(FormInterface $form, User $user)
    {
        // encode the plain password
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                $form->get('plainPassword')->getData()
            )
        );

        $user->setConfirmationCode($this->codeGenerator->getConfirmationCode());

        $logo = $form['company']['logo']->getData();

        if ($logo) {
            $newFilename = $this->companyService->loadFile($logo);
            $user->getCompany()->setLogo($newFilename);
        }

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        // do anything else you need here, like send an email
        $this->mailer->sendConfirmationMessage($user);
    }

    public function enableUSer(User $user)
    {
        $user->setEnabled(true);
        $user->setConfirmationCode('');

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}