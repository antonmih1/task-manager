<?php


namespace App\Service;


class CodeGenerator
{
    /**
     * @return string
     */
    public function getConfirmationCode()
    {
        return uniqid();
    }
}