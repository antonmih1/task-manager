<?php


namespace App\Service;


use App\Entity\Project;
use App\Entity\Task;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Entity\User;
use Symfony\Component\Form\FormInterface;

class TaskService
{
    /**
     * @var ParameterBagInterface
     */
    private $params;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager , ParameterBagInterface $params)
    {
        $this->params = $params;
        $this->entityManager = $entityManager;
    }

    /**
     * @param $repository
     * @param $projectId
     * @return Project|null
     */
    public function getProject($repository, $projectId)
    {
        $project = null;

        if ($projectId) {
            $project = $repository->find($projectId);
        }

        return $project;
    }

    /**
     * @param Task $task
     * @param User $user
     * @param Project $project
     */
    public function new(Task $task, User $user, Project $project)
    {
        $task->addAuthor($user);
        $task->setStatus(Task::STATUS_NEW);

        $task->setProject($project);

        if($user->getRole() == User::ROLE_USER)
        {
            $task->addUser($user);
        }

        $this->entityManager->persist($task);
        $this->entityManager->flush();
    }

    /**
     * @param Task $task
     * @param $departmentUsers
     * @param User $user
     */
    public function setLeaderDepartment(Task $task, $departmentUsers, User $user)
    {
        $manager = null;

        foreach ($departmentUsers as $departmentUser) {
            if ($departmentUser->getRole() == 'ROLE_MANAGER') {
                $manager = $departmentUser;
            }
        }

        $task->addAuthor($user);
        $task->addUser($manager);
        $task->setStatus(Task::STATUS_NEW);

        $this->entityManager->persist($task);
        $this->entityManager->flush();
    }

}