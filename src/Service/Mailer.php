<?php


namespace App\Service;


use App\Entity\User;
use Swift_Mailer;
use Twig\Environment;

class Mailer
{
    public const FROM_ADDRESS = 'dovydenkovyacheslav@gmail.com';
    /**
     * @var Swift_Mailer
     */
    private $mailer;
    /**
     * @var Environment
     */
    private $twig;

    public function __construct(Swift_Mailer $mailer, Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    public function sendConfirmationMessage(User $user)
    {
        $message = (new \Swift_Message('Confirmation register.'))
            ->setFrom(self::FROM_ADDRESS)
            ->setTo($user->getEmail());

        if($user->getRole() == User::ROLE_ADMIN) {
            $message->setBody($this->twig->render(
                'emails/registration.html.twig',
                ['user' => $user]
            ),
                'text/html'
            );
        } elseif ($user->getRole() != User::ROLE_ADMIN) {
            $message->setBody($this->twig->render(
                'emails/confirmEmployee.html.twig',
                ['user' => $user]
            ),
                'text/html'
            );
        }


        $this->mailer->send($message);
    }
}