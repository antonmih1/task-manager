<?php


namespace App\Service;


use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class LoadFileService
{
    private $params;

    /**
     * @param ParameterBagInterface $params
     */
    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    /**
     * @param $logo
     * @param $path
     * @return string
     */
    public function loadFile($logo, $path): string
    {
        $originalFilename = pathinfo($logo->getClientOriginalName(), PATHINFO_FILENAME);

        $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
        $newFilename = $safeFilename.'-'.uniqid().'.'.$logo->guessExtension();

        try {
            $logo->move(
                $path,
                $newFilename
            );
        } catch (FileException $e) {

        }

        return $newFilename;
    }
}