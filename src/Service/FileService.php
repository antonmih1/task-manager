<?php


namespace App\Service;


use App\Entity\File;
use App\Entity\Task;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\Security;

class FileService
{
    /**
     * @var ParameterBagInterface
     */
    private $params;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var Security
     */
    private $security;
    /**
     * @var LoadFileService
     */
    private $loadFileService;
    public function __construct(EntityManagerInterface $entityManager,
                                ParameterBagInterface $params,
                                Security $security,
                                LoadFileService $loadFileService)
    {
        $this->params = $params;
        $this->entityManager = $entityManager;
        $this->security = $security;
        $this->loadFileService = $loadFileService;
    }

    public function create(FormInterface $form, Task $task, File $file)
    {
        $pathFile = $form['path']->getData();
        $path = $this->params->get('file_directory');
        if ($file) {
            $newFilename = $this->loadFileService->loadFile($pathFile, $path);

            $file->setPath($newFilename);
        }
        $user = $this->security->getUser();
        $file->setAuthorFile($user);
        $file->setDateCreate(new \DateTime());
        $file->setTask($task);

        $this->entityManager->persist($file);
        $this->entityManager->flush();
    }
}