<?php


namespace App\Service;

use App\Entity\Project;
use App\Entity\Task;
use App\Entity\User;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;

class ProjectService
{
    /**
     * @var ParameterBagInterface
     */
    private $params;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var LoadFileService
     */
    private $loadFileService;

    public function __construct(EntityManagerInterface $entityManager,
                                ParameterBagInterface $params,
                                LoadFileService $loadFileService)
    {
        $this->params = $params;
        $this->entityManager = $entityManager;
        $this->loadFileService = $loadFileService;
    }

    /**
     * @param FormInterface $form
     * @param Project $project
     * @param User $user
     */
    public function newProject(FormInterface $form, Project $project, User $user)
    {
        $logo = $form['logo']->getData();
        $path = $this->params->get('project_logo_directory');
        if ($logo) {
            $newFilename = $this->loadFileService->loadFile($logo, $path);

            $project->setLogo($newFilename);
        }

        $project->addUser($user);
        $project->setCompany($user->getCompany());

        $this->entityManager->persist($project);
        $this->entityManager->flush();
    }

    /**
     * @param FormInterface $form
     * @param Project $project
     */
    public function editProject(FormInterface $form, Project $project, User $admin)
    {
        $logo = $form['logo']->getData();
        $path = $this->params->get('project_logo_directory');

        if ($logo) {
            $newFilename = $this->loadFileService->loadFile($logo, $path);

            $project->setLogo($newFilename);
        }

        $oldUsers = $project->getUsers();
        foreach ($oldUsers as $user) {
            $project->removeUser($user);
        }

        $project->addUser($admin);

        $departments = $project->getDepartment();
        foreach ($departments as $department) {
            foreach ($department->getUsers() as $user) {
                $project->addUser($user);
            }
        }

        $this->entityManager->persist($project);
        $this->entityManager->flush();
    }

    /**
     * @param $doTasks
     * @param $repository
     * @param $project
     * @param $user
     * @return array
     */
    public function tasksCount($repository, $project, $user)
    {
        $tasksCount = [
            'done' => 0,
            'checked' => 0
        ];

        $allTasks = $repository->getTasksCount(null, null, $project, $user);

        if ($allTasks) {
            $doneTasksCount = $repository->getTasksCount(null, Task::STATUS_DONE, $project, $user);
            $checkedTasksCount = $repository->getTasksCount(null, Task::STATUS_CHECKED, $project, $user);
            $tasksCount['done'] = $doneTasksCount * 100 / $allTasks;
            $tasksCount['checked'] = $checkedTasksCount * 100 / $allTasks;
        }

        return $tasksCount;
    }

    /**
     * @param FormInterface $form
     * @param Project $project
     */
    public function setUsers(FormInterface $form, Project $project)
    {
        $users = $form['users']->getData();
        $usersInCompany = $project->getCompany()->getUsers();

        foreach ($usersInCompany as $user) {
            if($user->getRole() == User::ROLE_ADMIN) {
                $users[] = $user;
            }
        }

        foreach ($users as $user) {
            $project->addUser($user);
        }

        $this->entityManager->persist($project);
        $this->entityManager->flush();
    }
}