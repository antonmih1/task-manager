<?php


namespace App\Service;


use App\Entity\Task;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminService
{
    /**
     * @var ParameterBagInterface
     */
    private $params;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager , ParameterBagInterface $params)
    {
        $this->params = $params;
        $this->entityManager = $entityManager;
    }

    /**
     * @param $company
     * @return array
     */
    public function tasksCount($repository, $company): array
    {
        return [
            'all' => $repository->getTasksCount($company),
            'inWork' => $repository->getTasksCount($company, Task::STATUS_IN_WORK),
            'done' => $repository->getTasksCount($company, Task::STATUS_DONE)
        ];
    }
}