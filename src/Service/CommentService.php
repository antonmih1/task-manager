<?php


namespace App\Service;


use App\Entity\Comment;
use App\Entity\Task;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\Security;

class CommentService
{
    /**
     * @var ParameterBagInterface
     */
    private $params;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var Security
     */
    private $security;
    public function __construct(EntityManagerInterface $entityManager,
                                ParameterBagInterface $params,
                                Security $security)
    {
        $this->params = $params;
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    public function create(FormInterface $form, Task $task, Comment $comment)
    {
        $user = $this->security->getUser();
        $comment->setAuthor($user);
        $comment->setDateCreate(new \DateTime());
        $comment->setTask($task);

        $comment->setDeleted(false);

        $this->entityManager->persist($comment);
        $this->entityManager->flush();
    }
}