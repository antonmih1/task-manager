<?php

namespace App\Form;

use App\Entity\Department;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('firstName', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a first name',
                    ]),
                    new Length([
                        'min' => 2,
                        'minMessage' => 'Your first name should be at least {{ limit }} characters',
                        'max' => 128,
                    ]),
                ]
            ])
            ->add('lastName', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a last name',
                    ]),
                    new Length([
                        'min' => 3,
                        'minMessage' => 'Your last name should be at least {{ limit }} characters',
                        'max' => 128,
                    ]),
                ]
            ])
            ->add('department', EntityType::class, [
                    'class' => Department::class,
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        return $er->createQueryBuilder('d')
                            ->where('d.company = :company')
                            ->setParameter('company', $options['admin']->getCompany());
                    },
                    'choice_label' => 'name',
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Please choice department.',
                            ])
                        ],
                ]
            )
            ->add('role', ChoiceType::class,
                ['choices'  => [
                            'Employee' => User::ROLE_USER,
                            'Manager'     => User::ROLE_MANAGER,
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'admin' => false,
        ]);

        $resolver->setAllowedTypes('admin', 'App\Entity\User');
    }
}
