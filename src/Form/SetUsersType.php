<?php

namespace App\Form;

use App\Entity\Project;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SetUsersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('users', EntityType::class, [
                'class' => User::class,
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('u')
                        ->where('u.company = :company')
                        ->setParameter('company', $options['admin']->getCompany())
                        ->innerJoin('u.department', 'd', 'WITH', 'd.id IN (:departments)')
                        ->setParameter('departments', $options['project']->getDepartment())
                        ->andWhere('u.enabled = :enabled')
                        ->setParameter('enabled', true);
                },
                'choice_label' => 'getFullName',
                'multiple' => true,
                'mapped' => true,
                'expanded' => true,
            ]);
        if($options['admin']->getRole() == User::ROLE_MANAGER) {
            $builder
                ->add('users', EntityType::class, [
                    'class' => User::class,
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        return $er->createQueryBuilder('u')
                            ->where('u.company = :company')
                            ->setParameter('company', $options['admin']->getCompany())
                            ->innerJoin('u.department', 'd', 'WITH', 'd.id IN (:departments)')
                            ->setParameter('departments', $options['project']->getDepartment())
                            ->andWhere('u.department = :department')
                            ->setParameter('department', $options['admin']->getDepartment())
                            ->andWhere('u.enabled = :enabled')
                            ->setParameter('enabled', true);

                    },
                    'choice_label' => 'getFullName',
                    'multiple' => true,
                    'mapped' => true,
                    'expanded' => true,
                ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Project::class,
            'admin' => null,
            'project' => null,
        ]);

        $resolver->setAllowedTypes('admin', 'App\Entity\User');
        $resolver->setAllowedTypes('project', 'App\Entity\Project');
    }
}
