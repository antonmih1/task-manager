<?php

namespace App\Form;

use App\Entity\Department;
use App\Entity\Project;
use App\Entity\Task;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaskLeaderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('startDate')
            ->add('endDate')
            ->add('project', EntityType::class, [
            'class' => Project::class,
            'choice_label' => 'name',
            ]);

//        $user = $options['user'];
//        if($user->getRole() != User::ROLE_USER) {
//            $builder
//                ->add('users', EntityType::class, [
//                    'class' => User::class,
//                    'choice_label' => 'getFullName',
//                    'multiple' => true,
//                ]);
//        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Task::class,
            'user' => false,
        ]);

        $resolver->setAllowedTypes('user', 'App\Entity\User');
    }
}
