<?php

namespace App\Form;

use App\Entity\Project;
use App\Entity\Task;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class TaskType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a task name',
                    ]),
                    new Length([
                        'min' => 3,
                        'max' => 128,
                    ]),
                ]
            ])
            ->add('description', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a task description',
                    ]),
                    new Length([
                        'min' => 3,
                        'max' => 512,
                    ]),
                ]
            ])
            ->add('startDate', DateType::class)
            ->add('endDate', DateType::class);

        $user = $options['user'];
        if($user->getRole() == User::ROLE_ADMIN) {
            $builder
                ->add('users', EntityType::class, [
                    'class' => User::class,
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        $qb = $er->createQueryBuilder('u');
                        $qb
                            ->where('u.company = :company')
                            ->setParameter('company', $options['user']->getCompany())
                            ->andWhere(':project MEMBER OF u.projects')
                            ->setParameter('project', $options['project']);

                        return $qb;
                    },
                    'choice_label' => 'getFullName',
                    'multiple' => true,
                    'expanded' => true,
                ]);
        }
        if($user->getRole() == User::ROLE_MANAGER) {
            $builder
                ->add('users', EntityType::class, [
                    'class' => User::class,
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        $qb = $er->createQueryBuilder('u');
                        $qb
                            ->where('u.company = :company')
                            ->setParameter('company', $options['user']->getCompany())
                            ->andWhere(':project MEMBER OF u.projects')
                            ->setParameter('project', $options['project'])
                            ->andWhere('u.department = :department')
                            ->setParameter('department', $options['user']->getDepartment());

                        return $qb;
                    },
                    'choice_label' => 'getFullName',
                    'multiple' => true,
                    'expanded' => true,
                ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Task::class,
            'user' => null,
            'project' => null,
        ]);

        $resolver->setAllowedTypes('user', 'App\Entity\User');
        $resolver->setAllowedTypes('project', 'App\Entity\Project');
    }
}
