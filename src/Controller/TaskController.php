<?php

namespace App\Controller;

use App\Entity\Department;
use App\Entity\Project;
use App\Entity\Task;
use App\Form\TaskLeaderType;
use App\Form\TaskType;
use App\Security\Voter\TaskVoter;
use App\Service\TaskService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tasks")
 */
class TaskController extends AbstractController
{

    private $taskService;

    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    /**
     * @Route("/", name="task_index")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        $name = $request->query->get('name');
        $status = $request->query->get('status');
        $projectId = $request->query->get('project');

        $repository = $this->getDoctrine()
            ->getRepository(Project::class);
        $project = $this->taskService->getProject($repository, $projectId);

        $repository = $this->getDoctrine()
            ->getRepository(Task::class);
        $user = $this->getUser();
        $company = $user->getCompany();
        $projects = $user->getProjects();
        $tasks = $repository->getTasks($company, $status, $project, $user, $name);

        return $this->render('task/index.html.twig', [
            'tasks' => $tasks,
            'user' => $user,
            'projects' => $projects,
        ]);
    }

    /**
     * @Route("/{id}/new", name="task_new", methods={"GET","POST"})
     * @param Request $request
     * @param Project $project
     * @return Response
     */
    public function new(Project $project, Request $request): Response
    {
        $user = $this->getUser();
        $task = new Task();
        $form = $this->createForm(TaskType::class, $task, [
            'user' => $user,
            'project' => $project,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->taskService->new($task, $user, $project);
            return $this->redirectToRoute('project_show', ['id' => $project->getId()]);
        }

        return $this->render('task/new.html.twig', [
            'task' => $task,
            'project' => $project,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="task_new_leader", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function setLeaderDepartment(Request $request): Response
    {
        $user = $this->getUser();
        $departments = $user->getCompany()->getDepartments();

        $task = new Task();
        $form = $this->createForm(TaskLeaderType::class, $task, [
            'user' => $user,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $department = $em->getRepository(Department::class)->find($request->get('department'));
            $departmentUsers = $department->getUsers();
            $this->taskService->setLeaderDepartment($task, $departmentUsers, $user);

            return $this->redirectToRoute('project_index');
        }

        return $this->render('task/new.leader.html.twig', [
            'task' => $task,
            'departments' => $departments,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="task_show", methods={"GET"})
     * @param Task $task
     * @return Response
     */
    public function show(Task $task): Response
    {
        return $this->render('task/show.html.twig', [
            'task' => $task,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="task_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Task $task
     * @return Response
     */
    public function edit(Request $request, Task $task): Response
    {
        $this->denyAccessUnlessGranted(TaskVoter::EDIT, $task);
        $form = $this->createForm(TaskType::class, $task, [
            'user' => $this->getUser(),
            'project' => $task->getProject(),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('project_show', ['id' => $task->getProject()->getId()]);
        }

        return $this->render('task/edit.html.twig', [
            'task' => $task,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN", message="No access! Get out!")
     * @Route("/{id}", name="task_delete", methods={"DELETE"})
     * @param Request $request
     * @param Task $task
     * @return Response
     */
    public function delete(Request $request, Task $task): Response
    {
        $this->denyAccessUnlessGranted(TaskVoter::DELETE, $task);
        if ($this->isCsrfTokenValid('delete' . $task->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($task);
            $entityManager->flush();
        }

        return $this->redirectToRoute('project_show', ['id' => $task->getProject()->getId()]);
    }

    /**
     * @Route("/accept/{id}", name="task_accept", methods={"GET"})
     * @param Task $task
     * @return Response
     */
    public function accept(Task $task): Response
    {
        $task->setStatus(Task::STATUS_IN_WORK);

        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('project_show', ['id' => $task->getProject()->getId()]);
    }

    /**
     * @Route("/checked/{id}", name="task_checked", methods={"GET"})
     * @param Task $task
     * @return Response
     */
    public function checked(Task $task): Response
    {
        $task->setStatus(Task::STATUS_CHECKED);

        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('project_show', ['id' => $task->getProject()->getId()]);
    }

    /**
     * @IsGranted("ROLE_USER", message="No access! Get out!")
     * @Route("/done/{id}", name="task_done", methods={"GET"})
     * @param Task $task
     * @return Response
     */
    public function done(Task $task): Response
    {
        $task->setStatus(Task::STATUS_DONE);

        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('project_show', ['id' => $task->getProject()->getId()]);
    }

}
