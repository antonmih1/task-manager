<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Project;
use App\Entity\Task;
use App\Form\CommentType;
use App\Service\CommentService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/comment")
 */
class CommentController extends AbstractController
{
    /**
     * @var CommentService
     */
    private $commentService;

    public function __construct(CommentService $commentService)
    {
        $this->commentService = $commentService;
    }

    /**
     * @Route("/comment", name="comment")
     */
    public function index()
    {
        return $this->render('comment/index.html.twig', [
            'controller_name' => 'CommentController',
        ]);
    }

    /**
     * @Route("/new/{id}", name="comment_new", methods={"GET","POST"})
     * @param Request $request
     * @param Task $task
     * @return Response
     * @throws \Exception
     */
    public function new(Request $request, Task $task): Response
    {
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->commentService->create($form, $task, $comment);

            return $this->redirectToRoute('project_show', ['id' => $task->getProject()->getId()]);
        }

        return $this->render('comment/new.html.twig', [
            'comment' => $comment,
            'task' => $task,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @IsGranted("ROLE_MANAGER", message="No access! Get out!")
     * @Route("/delete/{id}", name="comment_delete", methods={"GET","POST"})
     * @param Request $request
     * @param Comment $comment
     * @return Response
     */
    public function delete(Request $request,  Comment $comment): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $comment->setDeleted(true);

        $entityManager->persist($comment);
        $entityManager->flush();

        $idProject = $comment->getTask()->getProject()->getId();

        return $this->redirectToRoute('project_show', ['id' => $idProject]);
    }
}
