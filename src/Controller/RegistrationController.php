<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ChangePasswordType;
use App\Form\RegistrationFormType;
use App\Security\UserAuthenticator;
use App\Service\CodeGenerator;
use App\Service\CompanyService;
use App\Service\Mailer;
use App\Service\RegistrationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class RegistrationController extends AbstractController
{

    /**
     * @var RegistrationService
     */
    private $registrationService;
    /**
     * @var CompanyService
     */
    private $companyService;

    public function __construct(CompanyService $companyService,
                                RegistrationService $registrationService)
    {
        $this->companyService = $companyService;
        $this->registrationService = $registrationService;
    }
    /**
     * @Route("/register", name="app_register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param GuardAuthenticatorHandler $guardHandler
     * @param UserAuthenticator $authenticator
     * @param Mailer $mailer
     * @param CodeGenerator $codeGenerator
     * @return Response
     */
    public function register(Request $request,
                             UserPasswordEncoderInterface $passwordEncoder,
                             GuardAuthenticatorHandler $guardHandler,
                             UserAuthenticator $authenticator,
                             Mailer $mailer,
                             CodeGenerator $codeGenerator): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->registrationService->register($form, $user);

            return $this->render('registration/successRegister.html.twig');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/email_confirm", name="app_email_confirm")
     * @param Request $request
     * @return RedirectResponse|NotFoundHttpException
     */
    public function emailConfirm(Request $request)
    {
        $code = $request->query->get('code');

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['confirmationCode' => $code]);

        if(!$user) {
            return new NotFoundHttpException('User not found');
        }else {
            $this->registrationService->enableUSer($user);

            return $this->redirectToRoute('app_login');
        }
    }

    /**
     * @Route("/user_confirm", name="user_confirm")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return RedirectResponse|NotFoundHttpException
     */
    public function confirm(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $code = $request->query->get('code');

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['confirmationCode' => $code]);

        if(!$user) {
            return new NotFoundHttpException('User not found');
        } else {
            $form = $this->createForm(ChangePasswordType::class, $user);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $form->get('plainPassword')->getData()
                    )
                );

                $this->registrationService->enableUSer($user);

                return $this->redirectToRoute('app_login');
            }

            return $this->render('user/changePassword.html.twig', [
                'form' => $form->createView(),
            ]);
        }

    }
}
