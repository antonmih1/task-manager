<?php

namespace App\Controller\Admin;

use App\Entity\Task;
use App\Entity\User;
use App\Form\UserType;
use App\Service\AdminService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    private $adminService;

    public function __construct(AdminService $adminService)
    {
        $this->adminService = $adminService;
    }

    /**
     * @Route("/", name="admin")
     */
    public function index() :Response
    {
        $repository = $this->getDoctrine()
            ->getRepository(Task::class);

        $company = $this->getUser()->getCompany();
        $departments = $company->getDepartments();
        $users = $company->getEnabledUsers();
        $tasks = $this->adminService->tasksCount($repository, $company);

        return $this->render('admin/index.html.twig', compact('departments', 'company', 'users', 'tasks'));
    }

}
