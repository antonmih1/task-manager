<?php

namespace App\Controller\Admin;

use App\Entity\Company;
use App\Form\CompanyAdminType;
use App\Security\Voter\CompanyVoter;
use App\Service\CompanyService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/company")
 */
class CompanyController extends AbstractController
{
    private  $companyService;

    public function __construct(CompanyService $companyService)
    {
        $this->companyService = $companyService;
    }

    /**
     * @Route("/{id}/edit", name="company_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Company $company
     * @return Response
     */
    public function edit(Request $request, Company $company): Response
    {
        $this->denyAccessUnlessGranted(CompanyVoter::EDIT, $company);

        $form = $this->createForm(CompanyAdminType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->companyService->editCompany($form, $company);

            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/company/edit.html.twig', [
            'company' => $company,
            'form' => $form->createView(),
        ]);
    }
}
