<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\Task;
use App\Form\ProjectType;
use App\Form\SetUsersType;
use App\Security\Voter\ProjectVoter;
use App\Service\ProjectService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/project")
 */
class ProjectController extends AbstractController
{
    private $projectService;

    public function __construct(ProjectService $projectService)
    {
        $this->projectService = $projectService;
    }

    /**
     * @Route("/", name="project_index", methods={"GET"})
     */
    public function index(): Response
    {
        $user = $this->getUser();
        $projects = $user->getProjects();

        return $this->render('project/index.html.twig', [
            'projects' => $projects,
            'user' => $user,
        ]);
    }

    /**
     * Require ROLE_ADMIN for only this controller method.
     * @IsGranted("ROLE_ADMIN", message="No access! Get out!")
     * @Route("/new", name="project_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $project = new Project();
        $user = $this->getUser();

        $form = $this->createForm(ProjectType::class, $project, [
            'user' => $user,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->projectService->newProject($form, $project, $user);

            return $this->redirectToRoute('project_index');
        }

        return $this->render('project/new.html.twig', [
            'project' => $project,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="project_show", methods={"GET"})
     * @param Project $project
     * @return Response
     */
    public function show(Project $project, Request $request): Response
    {
        $this->denyAccessUnlessGranted(ProjectVoter::EDIT, $project);
        $name = $request->query->get('name');
        $status = $request->query->get('status');
        $repository = $this->getDoctrine()
            ->getRepository(Task::class);

        $user = $this->getUser();
        $doTasks = $repository->getTasks(null, $status, $project, $user, $name);
        $authorTasks = $repository->getThisProjectAndAuthorTasks($project, $user, $name, $status);
        $tasksCount = $this->projectService->tasksCount($repository, $project, $user);

        return $this->render('project/show.html.twig', [
            'project' => $project,
            'doTasks' => $doTasks,
            'user' => $user,
            'authorTasks' => $authorTasks,
            'tasksCount' => $tasksCount,
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN", message="No access! Get out!")
     * @Route("/{id}/edit", name="project_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Project $project
     * @return Response
     */
    public function edit(Request $request, Project $project): Response
    {
        $this->denyAccessUnlessGranted(ProjectVoter::EDIT, $project);

        $user = $this->getUser();

        $form = $this->createForm(ProjectType::class, $project, [
            'user' => $user,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->projectService->editProject($form, $project, $user);

            return $this->redirectToRoute('project_index');
        }

        return $this->render('project/edit.html.twig', [
            'project' => $project,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN", message="No access! Get out!")
     * @Route("/{id}", name="project_delete", methods={"DELETE"})
     * @param Request $request
     * @param Project $project
     * @return Response
     */
    public function delete(Request $request, Project $project): Response
    {
        $this->denyAccessUnlessGranted(ProjectVoter::EDIT, $project);

        if ($this->isCsrfTokenValid('delete' . $project->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($project);
            $entityManager->flush();
        }

        return $this->redirectToRoute('project_index');
    }

    /**
     * @IsGranted("ROLE_MANAGER", message="No access! Get out!")
     * @Route("/set-users/{id}", name="project_set_users", methods={"GET","POST"})
     * @param Request $request
     * @param Project $project
     * @return Response
     */
    public function setUsers(Request $request, Project $project): Response
    {
        $this->denyAccessUnlessGranted(ProjectVoter::EDIT, $project);

        $form = $this->createForm(SetUsersType::class, $project, [
            'admin' => $this->getUser(),
            'project' => $project,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->projectService->setUsers($form, $project);

            return $this->redirectToRoute('project_index');
        }

        return $this->render('project/setUsers.html.twig', [
            'project' => $project,
            'form' => $form->createView(),
        ]);
    }
}
