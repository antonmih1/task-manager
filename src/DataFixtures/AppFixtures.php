<?php

namespace App\DataFixtures;

use App\Entity\Company;
use App\Entity\Department;
use App\Entity\Project;
use App\Entity\Task;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function registerAdmin(string $companyName, string $email, string $name)
    {
        //Register user admin
        $company = new Company();
        $company->setName($companyName);

        $user = new User();
        $user->setRole(User::ROLE_ADMIN);
        $user->setCompany($company);
        $user->setConfirmationCode('');
        $user->setEmail($email);
        $user->setFirstName($name);
        $user->setLastName($name);
        $user->setEnabled(true);
        $user->setDepartment(null);
        $user->setPassword($this->encoder->encodePassword($user, '123456'));

        return $user;
        //End register user admin
    }

    public function createDepartment(string $departmentName, Company $company)
    {
        $department = new Department();
        $department->setName($departmentName);
        $department->setCompany($company);

        return $department;
    }

    public function addUser(string $role, Company $company, string $email, string $name, Department $department)
    {
        $user = new User();
        $user->setRole($role);
        $user->setCompany($company);
        $user->setConfirmationCode('');
        $user->setEmail($email);
        $user->setFirstName($name);
        $user->setLastName($name);
        $user->setEnabled(true);
        $user->setDepartment($department);
        $user->setPassword($this->encoder->encodePassword($user, $name));

        return $user;
    }

    public function createProject(string $name, string $description, Company $company, Department $department)
    {
        $project = new Project();
        $project->setName($name);
        $project->setDescription($description);
        $project->setCompany($company);
        $project->setDepartment($department);

        return $project;
    }

    public function createTask(string $name, string $description)
    {
        $format = 'Y-m-d H:i:s';

        $task = new Task();
        $task->setName($name);
        $task->setDescription($description);
        $task->setStatus(Task::STATUS_NEW);
        //$task->setStartDate(\DateTime::createFromFormat('\'Y-m-d H:i:s\'','2019-12-18 0:0:0'));
        $task->setStartDate(DateTime::createFromFormat($format,'2019-12-19 00:00:00'));
        $task->setEndDate(DateTime::createFromFormat($format,'2019-12-27 00:00:00'));

        return $task;
    }

    public function load(ObjectManager $entityManager)
    {

        //Register user admin
        $admin = $this->registerAdmin('Company1', 'company1admin@mail.ru', 'Company1Admin');
        $entityManager->persist($admin);
        //End register user admin

        //Add department to company
        $department1 = $this->createDepartment('DevOps', $admin->getCompany());
        $entityManager->persist($department1);

        $department2 = $this->createDepartment('Develop', $admin->getCompany());
        $entityManager->persist($department2);
        //End add department to company

        //Add employee and manager
        $employee = $this->addUser(User::ROLE_USER, $admin->getCompany(), 'company1user@mail.ru', 'Company1User', $department2);
        $entityManager->persist($employee);

        $manager = $this->addUser(User::ROLE_MANAGER, $admin->getCompany(), 'company1manager@mail.ru', 'Company1Manager', $department2);
        $entityManager->persist($manager);

        $employee1 = $this->addUser(User::ROLE_USER, $admin->getCompany(), 'company1user1@mail.ru', 'Company1User1', $department1);
        $entityManager->persist($employee1);

        $manager1 = $this->addUser(User::ROLE_MANAGER, $admin->getCompany(), 'company1manager1@mail.ru', 'Company1Manager1', $department1);
        $entityManager->persist($manager1);
        //End add employee and manager

        //Add project to company
        $project = $this->createProject('Project 1', 'Description', $admin->getCompany(), $department2);
        $project->addUser($admin);
        $project->addUser($employee);
        $project->addUser($manager);
        $entityManager->persist($project);
        //End add project to company

        //Add task to project
        $task = $this->createTask('Task 1', 'Description');
        $task->addAuthor($admin);
        $task->addUser($manager);
        $task->setProject($project);
        $entityManager->persist($task);
        //End add task to project


        $entityManager->flush();
    }
}
