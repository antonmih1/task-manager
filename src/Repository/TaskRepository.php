<?php

namespace App\Repository;

use App\Entity\Company;
use App\Entity\Project;
use App\Entity\Task;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }

    /**
     * @param Project $project
     * @param User $user
     * @param null $name
     * @param null $status
     * @return mixed
     */

    public function getThisProjectAndAuthorTasks(Project $project, User $user, $name = null, $status = null)
    {
        $qb = $this->createQueryBuilder('t')
            ->orderBy('t.endDate', 'ASC');

        $qb = $qb
            ->andWhere('t.project = :project')
            ->andWhere(':author MEMBER OF t.authors')
            ->setParameter('project', $project)
            ->setParameter('author', $user);

        if ($status) {
            $qb = $qb
                ->andWhere('t.status = :status')
                ->setParameter('status', $status);
        }

        if ($name) {
            $qb = $qb
                ->andWhere('t.name = :name')
                ->setParameter('name', $name);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Company|null $company
     * @param null $status
     * @param null $name
     * @param Project|null $project
     * @param User|null $user
     * @return mixed
     */

    public function getTasks(Company $company = null, $status = null, Project $project = null, User $user = null, $name = null)
    {
        $qb = $this->createQueryBuilder('t')
            ->orderBy('t.endDate', 'ASC');

        if ($company) {
            $qb = $qb
                ->innerJoin('t.authors', 'a', 'WITH', 'a.company = :company')
                ->setParameter('company', $company);
        }

        if ($status) {
            $qb = $qb
                ->andWhere('t.status = :status')
                ->setParameter('status', $status);
        }

        if ($name) {
            $qb = $qb
                ->andWhere('t.name = :name')
                ->setParameter('name', $name);
        }

        if ($project) {
            $qb = $qb
                ->andWhere('t.project = :project')
                ->setParameter('project', $project);
        }

        if ($user) {
            if ($user->getRole() == "ROLE_MANAGER") {
                $qb = $qb
                    ->innerJoin('t.users', 'u', 'WITH', 'u.department = :department')
                    ->setParameter('department', $user->getDepartment());
            }

            if ($user->getRole() == "ROLE_USER") {
                $qb = $qb
                    ->andWhere(':user MEMBER OF t.users')
                    ->setParameter('user', $user);
            }
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Company|null $company
     * @param null $status
     * @param Project|null $project
     * @param User|null $user
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getTasksCount(Company $company =  null, $status = null, Project $project = null, User $user = null)
    {
        $qb = $this->createQueryBuilder('t')
            ->select('count(t.id)');

        if ($company) {
            $qb = $qb
                ->innerJoin('t.authors', 'u', 'WITH', 'u.company = :company')
                ->setParameter('company', $company);
        }

        if ($status) {
            $qb = $qb
                ->andWhere('t.status = :status')
                ->setParameter('status', $status);
        }

        if ($project) {
            $qb = $qb
                ->andWhere('t.project = :project')
                ->setParameter('project', $project);

            if ($user->getRole() == "ROLE_MANAGER") {
                $qb = $qb
                    ->innerJoin('t.users', 'u', 'WITH', 'u.department = :department')
                    ->setParameter('department', $user->getDepartment());
            }

            if ($user->getRole() == "ROLE_USER") {
                $qb = $qb
                    ->andWhere(':user MEMBER OF t.users')
                    ->setParameter('user', $user);
            }
        }

        return $qb->getQuery()->getSingleScalarResult();
    }


}
