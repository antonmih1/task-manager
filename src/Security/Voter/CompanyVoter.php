<?php

namespace App\Security\Voter;

use App\Entity\Company;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class CompanyVoter extends Voter
{
    const EDIT = 'EDIT';

    protected function supports($attribute, $subject)
    {

        return in_array($attribute, [self::EDIT])
            && $subject instanceof \App\Entity\Company;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::EDIT:
                return $this->getPermission($user, $subject);
        }

        return false;
    }

    private function getPermission(User $user, Company $company)
    {
        return $user->getCompany()->getId() == $company->getId();
    }
}
