<?php

namespace App\Security\Voter;

use App\Entity\Department;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class DepartmentVoter extends Voter
{
    const EDIT = 'EDIT';
    const DELETE = 'DELETE';
    protected function supports($attribute, $subject)
    {
        return in_array($attribute, [self::EDIT, self::DELETE])
            && $subject instanceof \App\Entity\Department;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::EDIT:
                return $this->getPermission($user, $subject);
            case self::DELETE:
                return $this->getPermission($user, $subject);
        }

        return false;
    }

    private function getPermission(User $user, Department $department)
    {
        return $user->getCompany()->getId() == $department->getCompany()->getId();
    }
}
